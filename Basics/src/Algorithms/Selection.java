package Algorithms;

public class Selection implements Algorithm {

	
	  
	@Override
	public int doSort(int[] array) {
		// TODO Auto-generated method stub
		 for (int i = 0; i < array.length - 1; i++)  
	       {  
	           int temp = i;  
	           for (int j = i + 1; j < array.length; j++){  
	               if (array[j] < array[temp]){  
	                   temp = j;//searching for lowest index  
	               }  
	           }  
	           int smallerNumber = array[temp];   
	           array[temp] = array[i];  
	           array[i] = smallerNumber;  
	       }
		return 0;
		
	}  
   }  
	
	

