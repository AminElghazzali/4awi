package at.amin.cd;

import java.util.ArrayList;

public class CD {

	private String name;

	private ArrayList<Song> songs = new ArrayList<Song>();

	public CD(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Song> getSongs() {
		return songs;
	}

	public void setSongs(ArrayList<Song> songs) {
		this.songs = songs;
	}

	public void addSong(Song s) {
		songs.add(s);
	}

}
