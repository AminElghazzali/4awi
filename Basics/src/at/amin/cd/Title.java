package at.amin.cd;

import java.util.ArrayList;

public class Title implements Playable {

	private String title;
	private int length;
	private ArrayList<DVD> dvds = new ArrayList<DVD>();
	
	
	public Title(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}

	@Override
	public void play() {
		System.out.println("Played Song "+ title);
		
	}

	@Override
	public void pause() {
		System.out.println("Paused");
		
	}

	@Override
	public void stop() {
		System.out.println("Stopped");
		
	}

}
