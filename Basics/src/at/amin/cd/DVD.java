package at.amin.cd;

import java.util.ArrayList;

public class DVD implements Playable{

	private String Title;
	
	private ArrayList<Title> titles = new ArrayList<Title>();
	
	
	
	
	

	public DVD(String title) {
		super();
		Title = title;
	}

	@Override
	public void play() {
		System.out.println("Playing");
		
	}

	@Override
	public void pause() {
		System.out.println("Pause");
		
	}

	@Override
	public void stop() {
		System.out.println("Stop");
		
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public ArrayList<Title> getTitles() {
		return titles;
	}

	public void setTitles(ArrayList<Title> titles) {
		this.titles = titles;
	}
	
	public void addTitle(Title s)
	{
		titles.add(s);
	}
	
	
}
