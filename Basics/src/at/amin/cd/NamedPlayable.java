package at.amin.cd;

public abstract class NamedPlayable implements Playable{

	private String Title;

	
	
		
	public NamedPlayable(String title) {
		super();
		Title = title;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}
	
	
	
}
