package at.amin.cd;

import java.util.ArrayList;

public class Actor {

	private String firstName;
	private String latName;
	
	private ArrayList<CD> CDs = new ArrayList<CD>();
	private ArrayList<DVD> DVDs = new ArrayList<DVD>();
	
	public Actor(String firstName, String latName) {
		super();
		this.firstName = firstName;
		this.latName = latName;
	}
	
	  
	public void addCD(CD cd){
		
		CDs.add(cd);
	}
	 	

	
	public void addDVD(DVD dvd){
		
		DVDs.add(dvd);
	}


	public ArrayList<CD> getCDs() {
		return CDs;
	}


	public void setCDs(ArrayList<CD> cDs) {
		CDs = cDs;
	}


	public ArrayList<DVD> getDVDs() {
		return DVDs;
	}


	public void setDVDs(ArrayList<DVD> dVDs) {
		DVDs = dVDs;
	}
	
	
	
	
	

}
