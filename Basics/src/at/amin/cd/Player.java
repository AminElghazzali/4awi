package at.amin.cd;

import java.util.ArrayList;

public class Player {
	
	ArrayList<Playable> playables = new ArrayList<Playable>();
		
		public void addPlayable(Playable p) {
			playables.add(p);
		}
		
		public void playAll() {
			for(Playable p : playables) {
				p.play();
			}
		}
		
		public void pauseAll() {
			for(Playable p : playables) {
				p.pause();
			}
		}
		
		public void stopAll() {
			for(Playable p : playables) {
				p.stop();
			}
		}
}
