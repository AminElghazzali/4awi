package at.amin.cd;

import java.util.ArrayList;

public class Song implements Playable{

	
	private String title;
	private int length;
	private ArrayList<CD> cds = new ArrayList<CD>();
	
	
	@Override
	public void play() {
		System.out.println("Played Song " + title);
		
	}

	@Override
	public void pause() {
		System.out.println("Paused");
		
	}

	@Override
	public void stop() {
		System.out.println("Stopped");
		
	}

	
	
	public Song(String title, int length) {
		this.title = title;
		this.length = length;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public ArrayList<CD> getCds() {
		return cds;
	}

	public void setCds(ArrayList<CD> cds) {
		this.cds = cds;
	}
	
	
}
