package at.amin.rectangle;

public class Rectangle {

	private int a, b;

	public Rectangle(int b, int a) {
		this.a = a;
		this.b = b;

	}
	
	public int getA() {
		return this.a;
	}
	
	public int getB() {
		return this.b;
	}
	
	public void setA(int a) {
		this.a = a;
	}
	
	public void setB(int b) {
		this.b = b;
	}

	public void sayHello() {
		System.out.println("ich bin " + this.a + " breit und " + this.b + " lang");
	}

	public double getArea()
	{
		int a = this.a;
		int b = this.b;
		
		return a*b;
	}

	public void sayArea() {
		System.out.println("Der Fl�cheninhalt betr�gt " + getArea());
	}

	public double getCircumfrence() {
		
		return 2*(this.a + this.b);
	}
	public void sayCircumfrence() {
		System.out.println("Der Umfang betr�gt " + getCircumfrence());

}
}
