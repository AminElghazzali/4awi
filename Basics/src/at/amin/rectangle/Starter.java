package at.amin.rectangle;

public class Starter {

	public static void main(String[] args) {
		Rectangle r1 = new Rectangle(4, 2);
		Rectangle r2 = new Rectangle(7, 3);
		Rectangle r3 = r2;
		
		r1.sayHello();
		r2.sayHello();
		r3.sayHello();
		r1.sayArea();
		r1.sayCircumfrence();


	}

}
