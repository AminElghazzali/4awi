package at.amin.car;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

public class Starter {

	public static void main(String[] args) {

		
		Engine e1 = new Engine(true, 10000);
		manufacturer m1 = new manufacturer("VW", "DE", 50);
		Car c1 = new Car("white", 120, 50000, 7, 60000, 7, m1, e1);
		Car c2 = new Car("white", 120, 50000, 7, 60000, 7, m1, e1);
		
		
		
		
		Person p1 = new Person("hans", "Peter", LocalDate.of(1960, Month.JANUARY, 1));
		p1.addCar(c1);
		p1.addCar(c2);
		
		//System.out.println(c1.getusage());
		//System.out.println(c1.carPrice());
		//System.out.println(c1.getType());
		System.out.println(p1.getValueofCars());
		System.out.println(p1.getAge());
		System.out.println(c1.getColor());
		System.out.println(c1.getKm());
		
	}

}
