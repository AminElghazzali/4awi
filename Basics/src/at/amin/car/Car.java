package at.amin.car;

public class Car {
	
	private String color;
	private float topSpeed;
	private float price;
	private double usage;
	private float km;
	private double basicusage;
	private manufacturer manufacturer;
	private Engine engine;
	public Car(String color, float topSpeed, float price, double usage, float km, double basicusage, manufacturer manufacturer, Engine engine) {
		super();
		this.color = color;
		this.topSpeed = topSpeed;
		this.price = price;
		this.usage = usage;
		this.km = km;
		this.basicusage = basicusage;
		this.manufacturer = manufacturer;
		this.engine = engine;
		
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public float getTopSpeed() {
		return topSpeed;
	}
	public void setTopSpeed(float topSpeed) {
		this.topSpeed = topSpeed;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public double getUsage() {
		return usage;
	}
	public void setUsage(double usage) {
		this.usage = usage;
	}
	public float getKm() {
		return km;
	}
	public void setKm(float km) {
		this.km = km;
	}
	
	
	public double getusage()
	{
		if(this.km < 50000)
		{
			this.usage = this.basicusage;
		}
		else
		{
			this.usage = this.basicusage * 1.098;
		}
		
		return usage;
		
		
	}
	public double carPrice()
	{
		return (manufacturer.getDiscount() / 100)  * this.price;

	}
	public boolean getType()
	{
		
		return engine.isDiesel();
	}
	
	
	

}
