package at.amin.car;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person {

	private String firstName;
	private String lastName;
	LocalDate today = LocalDate.now();                          //Today's date
	LocalDate birthday;  //Birth date
	private Car car;
	private double price;
	
	
	private List<Car> cars;
	
	public Person(String firstName, String lastName, LocalDate birthday)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		
		
		this.cars = new ArrayList<>();
		
		
		
		
	}
	public void addCar(Car car)
	{
		
		this.cars.add(car);
		
	}
	public double getValueofCars()
	{
		
		for (Car car : cars) {
		price = price + car.carPrice();
		}
		
		return price;
	}
	
	public int getAge()
	{
		
		 
		Period p = Period.between(birthday, today);	
		 
		return p.getYears();
	}
	
	
	
}
