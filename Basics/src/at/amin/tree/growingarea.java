package at.amin.tree;

import java.util.List;

public class growingarea {
	
	private String name;
	private int size;
	
	private List<Tree> trees;
	
	public void addTree(Tree Tree)
	{
		this.trees.add(Tree);
		
		
	}
	
	
	public growingarea(String name, int size, List<Tree> trees) {
		super();
		this.name = name;
		this.size = size;
		this.trees = trees;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public void furtilizeAllTrees() {
		for (Tree tree : trees) {
			tree.getFertalize().fertalize();
		}
	
	}	
}