package at.amin.tree;

public class Tree {

	private int maxSize;
	private int maxDiameter;
	private fertalize_strategy fertalize;
	
	
	
	public Tree(int maxSize, int maxDiameter, fertalize_strategy fertalize) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.fertalize = fertalize;
		
	}
	
	
	public int getMaxSize() {
		return maxSize;
	}
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
	public int getMaxDiameter() {
		return maxDiameter;
	}
	public void setMaxDiameter(int maxDiameter) {
		this.maxDiameter = maxDiameter;
	}


	public fertalize_strategy getFertalize() {
		return fertalize;
	}


	public void setFertalize(fertalize_strategy fertalize) {
		this.fertalize = fertalize;
	}
	
	
	
	
}
