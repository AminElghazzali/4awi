package FactoryPattern;

import java.util.ArrayList;

public class ShapeFactory {

public static Shape CreateShape(String criteria) {
		
		if( criteria.equals("Rectangle"))
			return new Rectangle();
		else if (criteria.equals("Square"))
			return new Square();
		else if(criteria.equals("Circle"))
			return new Circle();
		
		
		return null;
		
	}
}
