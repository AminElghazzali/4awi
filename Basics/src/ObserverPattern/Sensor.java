package ObserverPattern;

import java.util.ArrayList;
import java.util.Observable;

public class Sensor {

	private String status;
	private ArrayList<ChristmasLights> components = new ArrayList<ChristmasLights>();

	public Sensor() {
		super();

	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<ChristmasLights> getComponents() {
		return components;
	}

	public void setComponents(ArrayList<ChristmasLights> components) {
		this.components = components;
	}

	public void addOberservable(ChristmasLights christmaslight) {
		components.add(christmaslight);

	}

	public void InformAll() {
		for (ChristmasLights christmaslight : this.components) {
			System.out.println("I am informed");
		}

	}
}
