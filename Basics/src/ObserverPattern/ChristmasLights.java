package ObserverPattern;

public class ChristmasLights implements Observable {

	@Override
	public void inform() {

		System.out.println("I am Informed");

	}

}
