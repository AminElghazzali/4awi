package ObserverPattern;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Sensor s1 = new Sensor();
		ChristmasLights c1 = new ChristmasLights();

		s1.addOberservable(c1);

		s1.InformAll();

	}

}
