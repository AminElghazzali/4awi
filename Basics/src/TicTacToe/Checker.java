package TicTacToe;

import java.util.Arrays;

public class Checker {

	static TicTacToe TicTacToe;
	
	static String checkWinner() {
		for (int a = 0; a < 8; a++) {
			String line = null;
			switch (a) {
			case 0:
				line = TicTacToe.field[0] + TicTacToe.field[1] + TicTacToe.field[2];
				break;
			case 1:
				line = TicTacToe.field[3] + TicTacToe.field[4] + TicTacToe.field[5];
				break;
			case 2:
				line = TicTacToe.field[6] + TicTacToe.field[7] + TicTacToe.field[8];
				break;
			case 3:
				line = TicTacToe.field[0] + TicTacToe.field[3] + TicTacToe.field[6];
				break;
			case 4:
				line = TicTacToe.field[1] + TicTacToe.field[4] + TicTacToe.field[7];
				break;
			case 5:
				line = TicTacToe.field[2] + TicTacToe.field[5] + TicTacToe.field[8];
				break;
			case 6:
				line = TicTacToe.field[0] + TicTacToe.field[4] + TicTacToe.field[8];
				break;
			case 7:
				line = TicTacToe.field[2] + TicTacToe.field[4] + TicTacToe.field[6];
				break;
			}
			if (line.equals("XXX")) {
				System.out.println("Congratulations X's have won!");
				return null;
				
				
			} else if (line.equals("OOO")) {
				System.out.println("Congratulations O's have won!");
				return null;
	
			}
		}

		for (int a = 0; a < 9; a++) {
			if (Arrays.asList(TicTacToe.field).contains(String.valueOf(a+1))) {
			break;
			}
			else if (a == 8) {
				System.out.println("It's a draw!");
				return null;
			}
			
		}

		System.out.println(TicTacToe.turn + "'s turn:");
		return null;
	}
	
	
	
	
}
