package TicTacToe;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;


		public class TicTacToe {
			public static Scanner input;
			public static String[] field;
			public static String turn;

			public static void main(String[] args) {
				
				Field.fillField();
				Field.printField();
				String winner = null;
				turn = "X";
				input = new Scanner(System.in);
				field= new String[9];
				System.out.println("X's will play first:");

				
				while (winner == null) {
					int numInput;
					
						numInput = input.nextInt();
						if (!(numInput > 0 && numInput <= 9)) {
							System.out.println("Invalid input; re-enter: ");
							continue;
						}
				
					if (field[numInput-1].equals(String.valueOf(numInput))) {
						field[numInput-1] = turn;
						if (turn.equals("X")) {
							turn = "O";
						} else {
							turn = "X";
						}
						Field.printField();
						winner = Checker.checkWinner();
					} else {
						System.out.println("Slot taken; re-enter: ");
						continue;
					}
				}
			}

		}
	
		




