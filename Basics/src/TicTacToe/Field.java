package TicTacToe;

public class Field {

	static TicTacToe TicTacToe;
	
	static void printField() {
		System.out.println("/---|---|---\\");
		System.out.println("| " + TicTacToe.field[0] + " | " + TicTacToe.field[1] + " | " + TicTacToe.field[2] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + TicTacToe.field[3] + " | " + TicTacToe.field[4] + " | " + TicTacToe.field[5] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + TicTacToe.field[6] + " | " + TicTacToe.field[7] + " | " + TicTacToe.field[8] + " |");
		System.out.println("/---|---|---\\");
	}

	static void fillField() {
		for (int a = 0; a < 9; a++) {
			TicTacToe.field[a] = String.valueOf(a+1);
		}
	}
	
}
