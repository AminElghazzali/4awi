package Encryption;

public class EncryptionMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String encryptedMessage = null;
		String decryptedMessage = null;
		
		String message = "Amin";
		
		
		int key = 4;
		Ceasar ceasar = new Ceasar();
		encryptedMessage = ceasar.Encrypt(message, key);
		decryptedMessage = ceasar.Decrypt(encryptedMessage, key);
		
		System.out.println("Encrypted Message = " + encryptedMessage);
		System.out.println("Decrypted Message = " + decryptedMessage);
	}

}
