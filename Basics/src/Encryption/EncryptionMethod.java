package Encryption;

public interface EncryptionMethod {
	
	public String Encrypt(String string);
		
	
	public String Decrypt(String string);
		
	
}
